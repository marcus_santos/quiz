<?php

namespace App\Services;

use App\Exceptions\QuizException;
use Illuminate\Session\SessionManager;

class QuizService
{

    const USER_SESSION = 'userAnswer';

    private $questions = [
        'flag_1' => [
            'question' => 'Question about flag 1?',
            'answers' => [
                ['id' => 1, 'answer' => 'Answer about flag 1 A', 'isCorrect' => false],
                ['id' => 2, 'answer' => 'Answer about flag 1 B', 'isCorrect' => false],
                ['id' => 3, 'answer' => 'Answer about flag 1 C', 'isCorrect' => true],
                ['id' => 4, 'answer' => 'Answer about flag 1 D', 'isCorrect' => false]
            ]

        ],
        'flag_2' => [
            'question' => 'Question about flag 2?',
            'answers' => [
                ['id' => 1, 'answer' => 'Answer about flag 2 A', 'isCorrect' => true],
                ['id' => 2, 'answer' => 'Answer about flag 2 B', 'isCorrect' => false],
                ['id' => 3, 'answer' => 'Answer about flag 2 C', 'isCorrect' => false],
                ['id' => 4, 'answer' => 'Answer about flag 2 D', 'isCorrect' => false]
            ]
        ],
        'flag_3' => [
            'question' => 'Question about flag 3?',
            'answers' => [
                ['id' => 1, 'answer' => 'Answer about flag 3 A', 'isCorrect' => false],
                ['id' => 2, 'answer' => 'Answer about flag 3 B', 'isCorrect' => true],
                ['id' => 3, 'answer' => 'Answer about flag 3 C', 'isCorrect' => false],
                ['id' => 4, 'answer' => 'Answer about flag 3 D', 'isCorrect' => false]
            ]
        ],
        'flag_4' => [
            'question' => 'Question about flag 4?',
            'answers' => [
                ['id' => 1, 'answer' => 'Answer about flag 4 A', 'isCorrect' => false],
                ['id' => 2, 'answer' => 'Answer about flag 4 B', 'isCorrect' => false],
                ['id' => 3, 'answer' => 'Answer about flag 4 C', 'isCorrect' => false],
                ['id' => 4, 'answer' => 'Answer about flag 4 D', 'isCorrect' => true]
            ]
        ],
        'flag_5' => [
            'question' => 'Question about flag 1?',
            'answers' => [
                ['id' => 1, 'answer' => 'Question about flag 5 A', 'isCorrect' => false],
                ['id' => 2, 'answer' => 'Question about flag 5 B', 'isCorrect' => false],
                ['id' => 3, 'answer' => 'Question about flag 5 C', 'isCorrect' => true],
                ['id' => 4, 'answer' => 'Question about flag 5 D', 'isCorrect' => false]
            ]
        ],
    ];
    /**
     * @var SessionManager
     */
    private $session;

    public function __construct(SessionManager $session)
    {

        $this->session = $session;
    }

    public function init()
    {
        $this->session->flush();
        $this->session->put(static::USER_SESSION, []);
    }

    public function getRandomIndex()
    {
        return mt_rand(0, count($this->questions) - 1);
    }


    public function getQuestionByFlagShuffled($index)
    {
        $this->verifyIndex($index);

        $key = $this->getKeyByIndex($index);
        $question = $this->questions[$key];
        shuffle($question['answers']);
        return $question;
    }

    public function isCorrect($index, $indexAnswer)
    {

        $this->verifyIndex($index);
        $key = $this->getKeyByIndex($index);
        return $this->questions[$key]['answer'][$indexAnswer]['isCorrect'];
    }

    /**
     * @param $index
     * @throws QuizException
     */
    protected function verifyIndex($index)
    {
        if ($index < 0 || $index >= count($this->questions)) {
            throw new QuizException('Questão inválida');
        }
    }

    /**
     * @param $index
     * @return mixed
     */
    protected function getKeyByIndex($index)
    {
        $key = array_keys($this->questions)[$index];
        return $key;
    }

    public function getUserAnswer($index)
    {
        $userAnswers = $this->session->get(static::USER_SESSION);
        $value = isset($userAnswers[$index])
            ? $userAnswers[$index]
            : null;
        //dd($userAnswers, $value);
        return $value;

    }

    public function setUserAnswer($data)
    {
        $answers = $this->session->get(static::USER_SESSION);
        $answers[$data['question']] = $data['answer'];
        $this->session->put(static::USER_SESSION, $answers);
    }

    public function getNextQuestionId($index)
    {
        if($this->finished() || ! $this->getUserAnswer($index)){
            return null;
        }
        $answered = array_keys($this->session->get(static::USER_SESSION));

        do {
            $rand = rand(0, count($this->questions) - 1);
        } while(in_array($rand, $answered));

        return $rand;

    }

    public function finished()
    {
        return count($this->session->get(static::USER_SESSION)) === 5;
    }

    public function questionsWithAnswers()
    {
        $questions = $this->questions;
        $answers = $this->session->get(static::USER_SESSION);
        foreach ($answers as $index => $value) {
            $questions[$this->getKeyByIndex($index)]['userAnswer'] = $value;
        }

        return $questions;
    }



}
