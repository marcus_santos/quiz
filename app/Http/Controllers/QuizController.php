<?php

namespace App\Http\Controllers;

use App\Exceptions\QuizException;
use App\Services\QuizService;
use Illuminate\Http\Request;

class QuizController extends Controller
{

    /**
     * @var QuizService
     */
    private $quizService;

    public function __construct(QuizService $quizService)
    {
        $this->quizService = $quizService;
    }

    public function index()
    {
        $this->quizService->init();


        return view('quiz.index',[
            'index' => $this->quizService->getRandomIndex()
        ]);
    }

    public function question($indexBandeira)
    {

        if($this->quizService->finished()) {
            return redirect()->route('summary');
        }
        try {
            $question = $this->quizService->getQuestionByFlagShuffled($indexBandeira);
        } catch (QuizException $e) {
            return redirect()->route('home');
        }

        return view('quiz.question',[
            'index' => $indexBandeira,
            'userAnswer' => $this->quizService->getUserAnswer($indexBandeira),
            'question' => $question,
            'nextQuestionId' => $this->quizService->getNextQuestionId($indexBandeira)
        ]);

    }

    public function answer(Request $request)
    {
        $request->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);

        $this->quizService->setUserAnswer($request->all());

        return redirect()->route('question', ['index' => $request->get('question')]);
    }

    public function summary()
    {
        if(!$this->quizService->finished()) {
            return redirect()->route('home');
        }

        return view('quiz.summary', [
           'questions' => $this->quizService->questionsWithAnswers(),
        ]);
    }
}
