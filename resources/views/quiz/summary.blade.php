@extends('layout.app')
@section('content')
    <h1>Resultado</h1>
    <a href="{{route('home')}}" class="btn btn-primary">Reponder novamente</a>
    @foreach($questions as $key => $question)
        <h4>{{$key}}</h4>
        <ul>
            @foreach($question['answers'] as $answer)
                <li>
                    @if( $answer['isCorrect'] )
                        <span class="text-success">
                        {{$answer['answer']}}
                    </span>
                    @elseif($answer['id'] == $question['userAnswer'] )
                        <span class="text-danger">
                        {{$answer['answer']}}
                    </span>
                    @else
                        {{$answer['answer']}}
                    @endif

                </li>
            @endforeach
        </ul>
    @endforeach

@stop
