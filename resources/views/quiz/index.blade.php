@extends('layout.app')
@section('content')
    <h1>Bem Vindo ao Quiz</h1>
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('question', ['index' => $index])}}" class="btn btn-primary">
                Iniciar
            </a>
        </div>
    </div>
@stop
