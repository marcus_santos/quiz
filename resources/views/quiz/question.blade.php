@extends('layout.app')
@section('content')

    <h3>{{$question['question']}}</h3>
    <form action="{{route('answer')}}" method="post">
        {{csrf_field()}}
        <input type="hidden" name="question" value="{{$index}}" />
        <ul>
        @foreach($question['answers'] as $answer)
            <li>

                <input type="radio" name="answer"
                       value="{{$answer['id']}}"
                        {{(!is_null($userAnswer) ? 'disabled': '' )}}
                        {{(!is_null($userAnswer) && $userAnswer == $answer['id'] ? 'checked': '' )}}

                />

                @if(!is_null($userAnswer) && $answer['isCorrect'] )
                <span class="text-success">
                    {{$answer['answer']}}
                </span>
                @elseif(!is_null($userAnswer) && $answer['id'] == $userAnswer )
                <span class="text-danger">
                    {{$answer['answer']}}
                </span>
                @else
                {{$answer['answer']}}
                @endif

            </li>
        @endforeach
        </ul>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(is_null($userAnswer))
        <button class="btn btn-primary" >Enviar</button>
        @else
        <a href="{{route('question', ['index' => $nextQuestionId])}}" class="btn btn-primary" >Continuar</a>
        @endif
    </form>
@stop
