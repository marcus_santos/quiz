<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'QuizController@index')->name('home');
Route::get('/bandeiras/{index}', 'QuizController@question')->name('question');
Route::post('/bandeiras', 'QuizController@answer')->name('answer');
Route::get('/summary', 'QuizController@summary')->name('summary');
